const express = require("express");
const router = express.Router();
const prodController = require("../controller/prodController.js");
const auth = require("../auth.js");

module.exports = router;